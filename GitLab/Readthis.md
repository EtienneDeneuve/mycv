# Gitlab  

Gitlab est une plateforme permettant d'echanger les elements relatifs aux projets.  

Repository= Dossier, projet.  

## Installation Gitlab

 * Ouvrir le terminal
  * `git init` => Entrer
  * Aller sur le site `https://confluence.atlassian.com/bitbucket/configure-your-dvcs-username-for-commits-950301867.html`  
  * Aller dans `Configure your Git username/email` et Copier les lignes **user name** et **user email** en veillant a mettre ses informations personnelles. 
 Copier les lignes et les coller sur le terminal puis valider. On valide chaque ligne a part.
  * Aller dans Webstorm => Preferences => Version control => Gitlad => Add new Gitlab server.
  * Remplir les chanps vides et le token. Ce dernier doit etre recupere depuis Gitlab dans Personnal Access Token => ok => ok.  
 
## Les Branches
 
 Master: Version reference du projet validée par le chef de projetg.  
 Branch: Copie du Master (Reference) à l'instant T permettant d'apporter des modifications sur un projet de groupe.
   
 Afin d'apporter des modifications à un projet:  
 * On crée une branche
 * On modifie le fichier sur lequel on travaille  
 * On COMMIT/push
 * On genere une Merge request
 * Les COMMIT sont envoyés au responsable du projet qui le bascule en master.
 
## Gitignore  
 
 Il sert à ignorer les dossiers/fichiers qu'on ne voudrait pas rendre public et faire apparaitre sur Gitlab tels les node-modules.  
 Pour l'installer, il suffir de créer un fichier `.gitignore` et rajouter les fichiers/dossiers qu'on veut ignorer en les rajoutant au fichier `.gitignore`.  
  
> A noter: l'ideal est de créé un `.gitignore` juste après `git init`. Les dossiers à ignorer se mettront ainsi automatiquement dans `.gitignore`.


