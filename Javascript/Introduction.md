# Introduction au Markdown

## Les titres 

* Le symbole **`#`** correspondau titre principal de la page. Il ne peut y avoir qu'un titre principal par page.
* Le symbole **`##`** correspond au titre niveau II.
* Le symbole **`###`** correspond au titre niveau III.

...ETC

## Comment se presente le code sur Markdown

* Si c'est une ligne de code: ` `` ` 
  * Ex: `const toto = 4`

* Si c'est plusieures lignes de codes: ` ``` ` au debut et a la fin du bout de code ( on ouvre et on ferme
) 

  * Ex:
```
    ```Javascript 
        const  toto=42 
        const  toto=42 
        const  toto=42
        const  toto=42
    ```
```

> PS: Le langage utilise peut etre indique juste apres le symbole ``` et ce en fonction des interpreters. Ceci est vivement recommande.

## La mise en forme du texte
* Pour ecrire en gras on utilise le symbole: `** **`
* Pour ecrire en Italique on utilise le symbole: `_ _`
* Pout ecrire une citation on utilise le symbole: `>` 

  * Ex: 

> salut 
> Comment va-t-on aujourd'hui?

PS: Il convient de sauter une ligne afin que les phrases ne soient pas collées.

* Pour saisir des listes a puce on utilise le symbole: `*`. Il doit y avoir un espace entre le `*` et le mot en question.

* Pour saisir des listes numerotees on utilise le symbole: `1.` Il doit y avoir un espace entre la puce numérotés et la phrase. En fonction du moteur de rendu, la numérotation se fait toute seules.

* Les sous listes sont presentees comme suit:  `* TEXT` ( espace espace* espace TEXT)

* pour inserer un lien on utilise les symboles: `[texte du lien](url de l'element)`

  * Ex: 
    [coucou](coucou.com)

  * Ex 2:
    ![img](https://img.ohmymag.com/article/480/inde/voici-le-chat-rubigineux_f3e8941069fda119913081c43660bc23bf5e6900.jpg)
