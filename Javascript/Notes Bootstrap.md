# Bootstrap

## Qu'est ce que Bootstrap ?

 * C'est un Framework html et css qui permets de structurer et d'encadrer notre facon de travailler.

## Installer Bootstrap 

 * Aller dans Terminal
 * Saisir : `npm init` => entrer =>entrer =>entrer => Name (MONCV) 
   * Un fichier `package.json` va se créer
 * Saisir : `npm install bootstrap` => Entrer
    * Un fichier `package-lock.json` va se créer

## Creer un nouveau fichier HTML dans WebStorm

* Main=>File=>New HTML FILE

On l'appelera index.html

### Activer Bootstrap

Saisir dans index.html: `<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.css">`

> il faut remonter d'un niveau pour aller sur Bootstrap d'ou les deux points ../ qui signfie par convention "dossier parents"
> On appuie sur control+espace et une liste d'autocompletion s'affiche. on peut ainsi sélectionner le fichier recherché plus facilement.

### Body

* On cree une DIV comportant une class principale "container'
  * On cree une grille compose de lignes (ROW) et de colonnes.

  ``` html
  div class="container">
          <div class="row">
              <div class="col numbers">1</div>
              <div class="col numbers">2</div>
              <div class="col numbers">3</div>
          </div>
  ```

* On rajoute un style qu'on appelera **numbers** afin de centrer les colonnes en creant un file Styles.css.

``` css
  .numbers{
         text-align: center;
     }
```

On rattache le fichier style.css en rajoutant une ligne link comme suit:

`<link rel="stylesheet" href="../asset/style.css">` 

On remonte d'un niveau vers ASSET puis STYLESHEET.
PS: On peut voir que les colonnes numbers sont centrees en inspectant la page qu'on ouvre via index.html sur le navigateur.

#Les containers

Il existe deux types de containers: 
* Un container standard qui reste fixe. 
* Un container fluid: Qui s'adapte å la page et prend toute sa largeur.

### Row et Col

Col et Row fonctionnent toujours ensemble: Pas de Row sans Col et vis versa.   
Row est l'element principal contenant des Col.   
Un element est toujours wrappé dans un Row qui a sa Col.  
Bootstrap contient 12 colonnes.  
Pour décaler une colonne vers la gauche on utilise la fonction: Offset. Ceci est possible lorsque la totalité des colinnes est inférieur à 12. 
Le contenu sert à faire apparaitre les couleurs des colonnes.  On peut utiliser HTML entities **`&nbsp`** pour mettre du contenu sans saisir du texte ou definir une height et width.  
Contrairement aux `ROW`, les `COL` ont une longueur par défaut, ce qui fait qu'on ne les considère pas comme vides.  

### Créer des liens hypertexte

Il faut utilidser la balise `<a> </>` et remoplir la valeur `href="..."` par le chemin approprié.  

Ex: 
```html
<a href="../mondossier/monfichier.xyz">TEXT to show</a>
```

> Note: 
> - `.` signifie dossier courant.  
> - `..` signifie dossier parent.








