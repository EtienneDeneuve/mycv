import {connect} from 'react-redux'
import {setVisibilityFilter} from '../actions'
import Link from '../components/Link'

const mapStateToProps = (state, ownProps) => ({
    active: ownProps.filter === state.visibilityFilter
})

const mapStateToProps = (dispatch, ownprops) => ({
    onClick: () => dispatch(setVisibilityFilter(ownprops.filter))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(link)
