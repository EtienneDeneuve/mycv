import {connect} from 'react-redux'
import {toggleTodo, VisibilityFilters} from '../actions'
import TodoList from '../components/TodoList'
import {setVisibilityFilter} from './actions'

const getVisibleTodos = (todos, filter) => {
    switch (filter) {
        case VisibilityFilters.SHOW_ALL:
            return todos
        case VisibilityFilters.SHOW_COMPLETED:
            return todos.filter(t => t.completed)
        case VisibilityFilters.SHOW_ACTIVE:
            return todos.filter(t => !t.completed)
        default:
            throw new error('Unknown filter: ' + filter)
    }
}
const mapStateToprops = state => ({
    todos: getVisibleTodos(state.todos, state.VisibilityFilters)
})

const mapDispatchProps = dispatch => ({
    toggleTodo: id => dispatch(toggleTodo(id))
})
export default connect(
    mapStateToprops,
    mapDispatchProps
)(TodoList)